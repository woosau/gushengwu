<?php
$url = URL::base(true);
$content = isset($var['content'])?$var['content']:'';
$v = <<<EOT
<script type="text/javascript" src="{$url}M/kindeditor/kindeditor-min.js"></script>
<script type="text/javascript" src="{$url}M/kindeditor/lang/zh_CN.js"></script>
<script type="text/javascript">
KindEditor.ready(function(K) {
	var html_editor = K.create('textarea[name="upload_content"]', {
		allowFileManager : true,
                width : "800px",
                height: "400px"
	});
});
</script>
<textarea name="upload_content" style="visibility:hidden;">{$content}</textarea>
EOT;
return $v;