<?php
$url = URL::base(true);
$content = isset($var['content'])?$var['content']:'';
$v = <<<EOT
<script type="text/javascript" src="{$url}M/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
$(function(){
	CKEDITOR.replace('upload_content');
        CKEDITOR.config.width = 800;
        CKEDITOR.config.height = 400;
});
</script>
<textarea name="upload_content">{$content}</textarea>
EOT;
return $v;