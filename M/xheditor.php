<?php
$url = URL::base(true);
$content = isset($var['content'])?$var['content']:'';
$v = <<<EOT
<script type="text/javascript" src="{$url}M/xheditor/xheditor-1.1.14-zh-cn.min.js"></script>
<script type="text/javascript">
$(function(){
	var html_editor = $('textarea[name="upload_content"]').xheditor({
		tools:'mfull',
                width:800,
                height:300
	});
});
</script>
<textarea name="upload_content">{$content}</textarea>
EOT;
return $v;