<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Login extends Controller {
	
	public function before(){
		parent::before();
	}
	
	public function after(){
		
		parent::after();
	}
	
	public function action_index()
	{
		if ($post = lib_security::killinject($this->request->post())){
			$session = Session::instance();
			//var_dump($post);
			//var_dump($_SESSION);exit;
			list($username,$password,$verifycode) = array($post['username'],md5($post['password']),$post['verifycode']);
			if (strtolower($verifycode) === strtolower($session->get('SafeCode'))){//验证码正确
				if (isset($_GET['action']) && $username && $password){
					$conn = SqliteDB::instance(Kohana::$config->load('conndb')->dbname);
					if('login' == $_GET['action']){//登录
						$sql = "select * from an_user where username='{$username}' and deleted!=1";
						$result = $conn->query($sql);
						if ($result && $password === $result[0]['password']){//有该账户并且密码正确
							
							//session记录
							$user = array (
								'id' => (int)$result [0] ['id'],
								'username' => (string)$result [0] ['username'],
								'power'=>(int)$result [0] ['power'],
								'powerName' => (string)Kohana::$config->load('website')->userPower[$result [0] ['power']],
								'ip' => (string)common::getip ()
							);
							$session->set ( 'user', ( object ) $user );
							
							//用户登录记录
							list($userId,$ip,$addTime) = array($result[0]['id'],common::getip(),time());
							$sql = "insert into an_login (userId,ip,addTime) values({$userId},'{$ip}',{$addTime})";
							$conn->query($sql);
							logunit::log(1,'用户登录',$sql,'loginId=>'.$conn->lastInsertRowid());
							
							//跳转
							/*if ('admin' == $result [0] ['username']) {//如果是总管理员账户
								project::redirect ( 'admin' );
							}
							project::redirect ( 'welcome' );*/
							project::redirect ( 'user' );
							//登陆成功！
						}else{
							$message = '用户名或者密码不正确！';
						}
					}elseif ('register' == $_GET['action']){//注册
						$sql = "select * from an_user where username='{$username}' and deleted!=1";
						$result = $conn->query($sql);
						if ($result){//用户名已存在
							$message = '该用户名已存在！';
						}else{
							list($ip,$addTime) = array(common::getip(),time());
							$sql = "insert into an_user (username,password,ip,addTime,deleted,power) values('{$username}','{$password}','{$ip}',{$addTime},0,4)";
							$conn->query($sql);
							$message = '注册成功！可以登录';
						}
					}
				}
			}else{
				$message = '验证码不正确！';
			}
			//var_dump($result);
		}
		echo View::factory('login/default')
		->set('username',isset($_POST['username'])?$_POST['username']:'')
		->set('message',isset($message)?$message:'');
	}
	
	public function action_logout(){
		Session::instance()->delete('user');
		project::redirect('welcome');
	}

} // End Welcome
