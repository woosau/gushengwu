<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller_Template {
	
	private $main,$left;
	
	public function before(){
		parent::before();
		
		if (TRUE === $this->auto_render){
			$this->template->styles = array_merge(array(), array(
            	'media/css/welcome.css'
            ));
            
			$this->template->title = '所内网页----中国科学院南京地质古生物研究所';
			$this->template->content = View::factory('welcome/index');
			
			//左侧栏目菜单
			$sql = "select * from an_resource where recommend=1";
			$resources = $this->_conn->query ( $sql );
			
			$this->left = View::factory('welcome/left')
			->set('object',$this)
			->set('columns',$this->columns)
			->set('resources',$resources);
		}
	}
	
	public function after(){
		if (TRUE === $this->auto_render){
			if (is_object($this->main)){
				$this->main
				->set('object',$this)
				->set('pagination',$this->pagination)
				->set('referer',$this->_session->get('ref')->HTTP_REFERER);
			}
			$this->template->content
			->set('left',$this->left)
			->set('main',$this->main);
			
			$this->html = array('head'=>false,'foot'=>false);
		}
		
		parent::after();
	}
	
	public function action_index()
	{
		project::redirect('welcome/recommend');
	}
	
	public function action_interface(){
		$this->main = View::factory('welcome/default');
	}
	
	public function action_columnopen(){
		if ($columnId = $this->request->param('id')){
			$sql = "select name from an_column where id={$columnId}";
			$result = $this->_conn->query($sql);
			$columnName = $result[0]['name'];
			$sql = "select * from an_resource where columnId={$columnId} order by addTime desc";
			$resources = $this->_conn->query($sql);
			
			$this->pagination = new Pagination(array(
				'total_items'=>count($resources)
			));
		}
		
		$this->main = View::factory('welcome/columnopen')
		->set('columnName',$columnName)
		->set('resources',array_slice($resources,$this->pagination->offset,$this->pagination->items_per_page));
	}
	
	public function action_resourceview(){
		if ($resourceId = $this->request->param('id')){
			$sql = "select * from an_resource where id=".$resourceId." limit 1";
			$result = $this->_conn->query($sql);
			$resource = $result[0];
			$resource['content'] = str_replace('\\','',$resource['content']);
			if ('art' != $resource['type']){
				die('非文章类型无法查看');
			}
		}
		
		$this->main = View::factory('welcome/resourceview')
		->set('resource',$resource);
	}
	
	public function action_url()
	{
		$sql = "select * from an_url order by position";
		$urls = $this->_conn->query($sql);
		
		$this->pagination = new Pagination(array(
			'total_items'=>count($urls)
		));
		
		$this->main = View::factory('welcome/url')
		->set('urls',array_slice($urls,$this->pagination->offset,$this->pagination->items_per_page));
	}
	
	public function action_recommend(){
		$sql = "select * from an_resource where recommend=1 order by addTime desc";
		$resources = $this->_conn->query ( $sql );
		
		$this->pagination = new Pagination ( array (
			'total_items' => count ( $resources )
		) );
		
		$this->main = View::factory('welcome/columnopen')
		->set('columnName','所内网页')
		->set('resources',array_slice($resources,$this->pagination->offset,$this->pagination->items_per_page));
	}

} // End Welcome
