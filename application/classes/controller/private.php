<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Private extends Controller_Template {
	
	private $main,$left;
	
	public function before(){
		parent::before();
	}
	
	public function after(){
		parent::after();
	}
	
	public function action_index()
	{
		;
	}
	
	//位置改变
	public function positionchange($what,$id,$direct){
		if ('down' == $direct){
			$logic = '>=';
			$order = '';
			//$positionchange = 1;
			$success = '排序已下降';
			$failed = '已经是最低位';
		}elseif ('up' == $direct){
			$logic = '<=';
			$order = ' desc';
			//$positionchange = -1;
			$success = '排序已上升';
			$failed = '已经是最高位';
		}
		
		$sql = "select position from an_{$what} where id={$id} limit 1";
		$result = $this->_conn->query($sql);
		$currentPosition = $result[0]['position'];
		$sql = "select id,position from an_{$what} where position{$logic}{$currentPosition} and id!={$id} order by position$order limit 1";
		$result = $this->_conn->query($sql);
		if ($result){
			$anotherId = $result[0]['id'];
			$newPosition = $result[0]['position'];
			//改变自身的位置
			$sql = "update an_{$what} set position={$newPosition} where id={$id}";
			$this->_conn->query($sql);
			//改变对立条目的位置
			$sql = "update an_{$what} set position={$currentPosition} where id={$anotherId}";
			$this->_conn->query($sql);
			Controller_Public::message($success);
		}else{
			Controller_Public::message($failed);
		}
	}
	
	public function action_action(){
		if ($strId = $this->request->param('id')){
			list($what,$how,$id) = explode('_',$strId);
			
			if ($id)
				$where = ' where id='.$id;
			else
				$where = '';
			
			$sql = '';
			switch ($what){
				case 'column':
					switch ($how){
						case 'del':
							$sql = "delete from an_column".$where;
							$action = '栏目删除';
							break;
						case 'positiondown':
							$this->positionchange($what,$id,'down');
							$action = '栏目排序下降';
							break;
						case 'positionup':
							$this->positionchange($what,$id,'up');
							$action = '栏目排序上升';
							break;
						default:$sql = '';
					}
					break;
				case 'resource':
					switch ($how){
						case 'del':
							$sql = "select * from an_resource".$where;
							$result = $this->_conn->query($sql);
							//var_dump($result);
							$resource = current($result);
							//$resource = current($this->_conn->query($sql));
							if (('art' !== $resource['type']) && is_file($resource['content'])){
								@unlink($resource['content']);
								if (is_file($resource['content'])){
									Controller_Public::message('资源无法删除');
									break;
								}
							}
							$sql = "delete from an_resource".$where;
							$action = '资源删除';
							break;
						default:$sql = '';
					}
					break;
				case 'user':
					if ('del' == $how) $sql = "update an_user set deleted=1".$where;
					$action = '用户删除';
					break;
				case 'url':
					switch ($how){
						case 'del':
							$sql = "delete from an_url".$where;
							$action = '常用网址删除';
							break;
						case 'positiondown':
							$this->positionchange($what,$id,'down');
							$action = '常用网址排序下降';
							break;
						case 'positionup':
							$this->positionchange($what,$id,'up');
							$action = '常用网址排序上升';
							break;
						default:$sql = '';
					}
					break;
				default:$sql = '';
			}
			
			Controller_Public::message($action.'成功',true);
			
			if (!empty($sql) && false === ($result = $this->_conn->query($sql))){
				die($this->_conn->last_error());
			}else{
				logunit::log($this->user->id,$action,$sql,$where);
			}
		}
		//var_dump($this->_session->get('ref'));exit;
		project::redirect($this->_session->get('ref')->HTTP_REFERER);
	}
	
	public function action_getColumns(){
		if ('admin' == $this->user->username){
			$controller = 'admin';
			$columnIds = array_keys($this->columns);
		}else{
			$controller = 'user';
			$columnIds = $this->columnPower[2];
		}
		
		$html = '';
		foreach ($columnIds as $columnId){
			$html .= "<div style='white-space:nowrap;'><a href='{$controller}/resourcemanage/{$columnId}'>{$this->columns[$columnId]['name']}</a></div>";
		}
		echo $html;
	}

} // End Welcome
