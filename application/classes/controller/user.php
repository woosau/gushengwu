<?php defined('SYSPATH') or die('No direct script access.');

class Controller_User extends Controller_Template {
	
	private $main,$left;
	
	public function before(){
		parent::before();
		
		if (0 === $this->user->power){
			project::redirect('login');
		}elseif (1 === $this->user->power){
			project::redirect('admin');
		}elseif (4 === $this->user->power){
			common::msg('非法进入！','/welcome');
		}
		
		if (TRUE === $this->auto_render){
			$this->template->title = $this->user->power;
			//$this->template->navigate[] = $this->user->powerName;
			$this->template->content = View::factory('user/index');
			$this->left = View::factory('user/left');
		}
	}
	
	public function after(){
		if (TRUE === $this->auto_render){
			if (is_object($this->main)){
				$this->main
				->set('object',$this)
				->set('pagination',$this->pagination)
				->set('referer',$this->_session->get('ref')->HTTP_REFERER);
			}
			$righttop = View::factory('common/righttop')
			->set('object',$this)
			->set('referer',$this->_session->get('ref')->HTTP_REFERER)
			->set('navigate',$this->template->navigate)
			->set('message',Controller_Public::messageShow());
			$this->template->content
			->set('left',$this->left)
			->set('righttop',$righttop)
			->set('main',$this->main);
		}
		
		parent::after();
	}
	
	public function action_index()
	{
		project::redirect('user/resourceupload');
	}
	
	public function action_resourceupload(){
		$this->template->navigate[] = '资源上传';
		set_time_limit(0);
		
		if (isset($_COOKIE['html_editor']))
                    $html_editor = $_COOKIE['html_editor'];
                else
                    $html_editor = 'kindeditor';
                    
		if ($post = $this->post){
			$success = true;
			if ($_FILES){
				$content = common::upload('upload_content');
				if (is_array($content)){
					$content = current($content);
					
					/*if ('mov' == $post['type']){
						$content = EncodeFlv::instance($content)->encode();
					}*/
				}else{
					$success = false;
					__404(new lib_exception($content));
					//Controller_Public::message($content);
				}
			}else{
				$content = addslashes($post['upload_content']);
			}
			
			if ($success){
				list($title,$columnId,$userId,$type,$addTime) = array($post['title'],$post['columnId'],$this->user->id,$post['type'],time());
				if (!in_array($columnId,$this->columnPower[2])){
					die('权限不足');
				}
				$sql = "insert into an_resource (title,columnId,userId,type,content,addTime) values('{$title}',{$columnId},{$userId},'{$type}','{$content}',{$addTime})";
				$this->_conn->query($sql);
				logunit::log($this->user->id,'资源上传',$sql,$title);
				Controller_Public::message('上传成功');
			}
		}
		
		$columns = array();
		foreach ($this->columnPower[2] as $columnId){
			$columns[$columnId] = $this->columns[$columnId]['name'];
		}
		//var_dump($columns);
		
		$this->main = View::factory('user/resourceupload')
		->set('columns',$columns)->set('html_editor',m::factory($html_editor));
	}
	
	public function action_resourcemanage(){
		$this->template->navigate[] = '栏目下属资源管理';
		
		$result = array();
		if ($columnId = $this->request->param('id')){
			if (in_array($columnId,$this->columnPower[1])){
				$level = 1;
			}elseif (in_array($columnId,$this->columnPower[2])){
				$level = 2;
			}else{
				die('权限不足');
			}
			$sql = "select name from an_column where id={$columnId}";
			$result = $this->_conn->query($sql);
			$columnName = $result[0]['name'];
			$sql = "select r.*,u.username from an_resource r left join an_user u on r.userId=u.id where r.columnId={$columnId} order by r.addTime desc";
			$result = $this->_conn->aliasClean($this->_conn->query($sql));
		}
		
		$this->main = View::factory('user/resourcemanage')
		->set('columnName',$columnName)
		->set('level',$level)
		->set('resources',$result);
	}
	
	public function action_resourceview(){
		$this->template->navigate[] = '资源查看/编辑';
		
		$resource = null;
		if ($resourceId = $this->request->param('id')){
			if ($post = $this->post){
				list($title,$content) = array($post['title'],$post['content']);
				$sql = "update an_resource set title='{$title}',content='{$content}' where id={$resourceId}";
				$this->_conn->query($sql);
				logunit::log($this->user->id,'资源编辑',$sql,$title);
				Controller_Public::message('编辑成功');
			}
			$sql = "select * from an_resource where id=".$resourceId." limit 1";
			$result = $this->_conn->query($sql);
			$resource = $result[0];
			
			if (in_array($resource['columnId'],$this->columnPower[1])){
				$level = 1;
			}elseif (in_array($resource['columnId'],$this->columnPower[2])){
				$level = 2;
			}else{
				die('权限不足');
			}
		}
		
		$sql = "select id,name from an_column order by position";
		$result = $this->_conn->query($sql);
		$columns = array();
		if ($result){
			foreach ($result as $result){
				$columns[$result['id']] = $result['name'];
			}
		}
		
		if (1 == $level || $resource['userId'] == $this->user->id){
			$this->main = View::factory('admin/resourceview')
			->set('columns',$columns)
			->set('resource',$resource);
		}elseif (2 == $level){
			$this->main = View::factory('user/resourceview')
			->set('columns',$columns)
			->set('resource',$resource);
		}
	}
	
	//访问记录
	public function action_login(){
		$this->template->navigate[] = '访问记录';
		
		$sql = "select u.username,u.power,l.ip,l.addTime from an_login l left join an_user u on l.userId=u.id where l.userId=".$this->user->id." order by l.addTime desc";
		$logins = $this->_conn->aliasClean($this->_conn->query($sql));
		
		$this->main = View::factory('user/login')
		->set('logins',$logins);
	}
	
	//操作日志 
	public function action_log(){
		$this->template->navigate[] = '操作日志';
		
		$sql = "select u.username,u.power,l.action,l.addTime from an_log l left join an_user u on l.userId=u.id where l.userId=".$this->user->id." order by l.addTime desc";
		$logs = $this->_conn->aliasClean($this->_conn->query($sql));
		
		$this->main = View::factory('user/log')
		->set('logs',$logs);
	}
	
	//修改密码
	public function action_passwordchange(){
		$this->template->navigate[] = '修改密码';
		
		if ($post = $this->post){
			if ($this->user->id !== $this->_session->get('user')->id){
				die('非法行为');
			}
			$sql = "select * from an_user where id={$this->user->id} and deleted!=1";
			$result = $this->_conn->query($sql);
			list($previousPass,$currentPass) = array(md5($post['previous']),md5($post['current']));
			if ($result && $previousPass === $result[0]['password']){//有该账户并且密码正确
				$sql = "update an_user set password='".$currentPass."' where id=".$this->user->id;
				$this->_conn->query($sql);
				Controller_Public::message('密码修改成功');
			}else{
				Controller_Public::message('信息错误');
			}
		}
		
		$this->main = View::factory('user/passwordchange');
	}

} // End Welcome
