<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Default extends Controller_Template {
	
	private $main,$left;
	
	public function before(){
		parent::before();
		
		if (TRUE === $this->auto_render){
			$this->template->title = '首页';
			$this->template->navigate = array('首页');
			$this->template->content = View::factory('default/index');
			$this->left = View::factory('default/left');
		}
	}
	
	public function after(){
		if (TRUE === $this->auto_render){
			if (is_object($this->main)){
				$this->main
				->set('object',$this)
				->set('pagination',$this->pagination)
				->set('referer',$this->_session->get('ref')->HTTP_REFERER);
			}
			$righttop = View::factory('common/righttop')
			->set('referer',$this->_session->get('ref')->HTTP_REFERER)
			->set('navigate',$this->template->navigate)
			->set('message',Controller_Public::messageShow());
			$this->template->content
			->set('left',$this->left)
			->set('righttop',$righttop)
			->set('main',$this->main);
		}
		
		parent::after();
	}
	
	public function action_index()
	{
		$array = array(1,234,3,534,534,534,34,45,566,7,6,);
		
		$this->pagination = new Pagination(array(
			'total_items'=>count($array)
		));
		
		$this->main = View::factory('default/default');
	}

} // End Welcome
