<?php defined('SYSPATH') or die('No direct script access.');

class Controller_404 extends Controller_Template {
	
	private $main,$left;
	
	public function before(){
		parent::before();
		
		if (TRUE === $this->auto_render){
			$this->template->title = $this->user->power;
			$this->template->navigate[] = '404报错信息';
			$this->template->content = View::factory('user/index');
			//$this->left = View::factory('user/left');
		}
	}
	
	public function after(){
		if (TRUE === $this->auto_render){
			if (is_object($this->main)){
				$this->main
				->set('object',$this)
				->set('pagination',$this->pagination)
				->set('referer',$this->_session->get('ref')->HTTP_REFERER);
			}
			$righttop = View::factory('common/righttop')
			->set('object',$this)
			->set('referer',$this->_session->get('ref')->HTTP_REFERER)
			->set('navigate',$this->template->navigate)
			->set('message',Controller_Public::messageShow());
			$this->template->content
			->set('left',$this->left)
			->set('righttop',$righttop)
			->set('main',$this->main);
		}
		
		parent::after();
	}
	
	public function action_index()
	{
		$post = $this->request->post();
		
		$this->main = View::factory('404/default')
	    ->set('exception',$post);
	}

} // End Welcome
