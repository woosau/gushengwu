<?php defined ( 'SYSPATH' ) or die ( 'No direct script access.' );

class Controller_Resource extends Controller
{
	public function action_media()
	{
		// Get the file path from the request
		$file = $this->request->param ( 'file' );
		
		// Find the file extension
		$ext = pathinfo ( $file, PATHINFO_EXTENSION );
		
		// Remove the extension from the filename
		$file = substr ( $file, 0, - (strlen ( $ext ) + 1) );
		
		if ($file = Kohana::find_file ( 'media', $file, $ext ))
		{
			// Send the file content as the response
			//$this->request->response = file_get_contents ( $file );
			$this->response->body(file_get_contents ( $file ));
		}
		else
		{
			// Return a 404 status
			$this->request->status = 404;
		}
		
		// Set the content type for this extension
		$this->request->headers ['Content-Type'] = File::mime_by_ext ( $ext );
	}
	
	
	public function action_upload()
	{
		// Get the file path from the request
		$file = $this->request->param ( 'file' );
		
		// Find the file extension
		$ext = pathinfo ( $file, PATHINFO_EXTENSION );
		
		// Remove the extension from the filename
		$file = substr ( $file, 0, - (strlen ( $ext ) + 1) );
		
		//if ($file = Kohana::find_file ( 'uploadFile', $file, $ext ))
		if (is_file($file = 'uploadFile/'.$file.'.'.$ext))
		{
            
			// Send the file content as the response
			//$this->request->response = file_get_contents ( $file );
			//echo "<script>location.href='/".$file."'</script>";
			//exit;
			header('Content-Description: File Transfer'); 
			header('Content-Type: application/octet-stream'); 
			header('Content-Disposition: attachment; filename='.basename($file)); 
			header('Content-Transfer-Encoding: binary'); 
			header('Expires: 0'); 
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
			header('Pragma: public'); 
			header('Content-Length: ' . filesize($file)); 
			ob_clean();
			flush();
			readfile($file);
			exit; 
			$this->response->body(file_get_contents ( $file ));
		}
		else
		{
			// Return a 404 status
			$this->request->status = 404;
			header('content-type:text/html;charset=utf8');
			die('找不到文件');
		}
		
		// Set the content type for this extension
		$this->request->headers ['Content-Type'] = File::mime_by_ext ( $ext );
	}
	
} // End Welcome Controller
