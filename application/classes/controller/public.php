<?php
class Controller_Public extends Controller
{
    private static $message;
    
    public function before(){        
        parent::before();
    }
    
    public function action_index(){
        ;
    }
    
    //提示信息赋值
    public static function message($value,$dontCover = false){
        $_session = session::instance();
        $message = empty(self::$message)?$_session->get('message'):self::$message;
        if (!$dontCover || empty($message)){
            self::$message = $value;
            $_session->set('message',$value);
        }
    }
    
    //提示信息提示
    public static function messageShow(){
        $_session = session::instance();
        $message = empty(self::$message)?$_session->get('message'):self::$message;
        $_session->delete('message');
        if (empty($message)){
        	$html = '<p style="display:none"></p>';
            return $html;
        }else{
        	//$html = '<p style="border:1px solid #355568;background:#8CAFC4;color:white;font-family:微软雅黑;font-size:18px;padding:5px;heigth:30px;width:250px;text-align:center;">'.$message.'</p>';
        	$html = '<div style="width:400px;border:3px solid #355568;"><div style="width:390px;border-bottom:1px dotted gray;padding:5px;font-size:14px;font-family:微软雅黑"><span style="font-weight:bold;color:red;">INFORMATION</span>系统信息</div><div style="width:390px;padding:10px;text-align:center;"><div style="margin:0px auto;width:300px;">'.$message.'</div></div></div>';
            return $html;
        }
    }
	
	public function action_verifycode() {
		include_once 'application/classes/verifycode.php';
	}
}