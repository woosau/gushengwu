<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin extends Controller_Template {
	
	private $main,$left;
	
	public function before(){
		parent::before();
		
		if ('admin' !== $this->user->username || 1 !== $this->user->id || 1 !== $this->user->power){
			common::msg('非法进入！','/login');
		}
		
		if (TRUE === $this->auto_render){
			$this->template->title = '管理员';
			$this->template->navigate[] = '管理员';
			$this->template->content = View::factory('admin/index');
			$this->left = View::factory('admin/left');
		}
	}
	
	public function after(){
		if (TRUE === $this->auto_render){
			if (is_object($this->main)){
				$this->main
				->set('object',$this)
				->set('pagination',$this->pagination)
				->set('referer',$this->_session->get('ref')->HTTP_REFERER);
			}
			$righttop = View::factory('common/righttop')
			->set('object',$this)
			->set('referer',$this->_session->get('ref')->HTTP_REFERER)
			->set('navigate',$this->template->navigate)
			->set('message',Controller_Public::messageShow());
			$this->template->content
			->set('left',$this->left)
			->set('righttop',$righttop)
			->set('main',$this->main);
		}
		
		parent::after();
	}
	
	public function action_index()
	{
		project::redirect('admin/usermanage');
	}
	
	public function action_usermanage(){
		$this->template->navigate[] = '会员管理';
		
		if ($post = $this->post){
			if (isset($post['username']) && $post['username']){
				$sql = "select id from an_user where username='{$post['username']}' and deleted!=1";
				if ($result = $this->_conn->query($sql)){
					Controller_Public::message('用户名已存在');
				}else{
					list($username,$password,$ip,$addTime,$power) = array(
						$post['username'],
						md5(Kohana::$config->load('conndb')->defaultUserPassword),
						'0.0.0.0',
						time(),
						4
					);
					$sql = "insert into an_user (username,password,ip,addTime,deleted,power) values('{$username}','{$password}','{$ip}',{$addTime},0,{$power})";
					if (false === $this->_conn->query($sql)){
						die($this->_conn->last_error());
					}else{
						logunit::log($this->user->id,'用户添加',$sql,$username);
					}
					project::redirect('admin/usermanage');
				}
			}
		}
		
		$sql = "select * from an_user where deleted=0 and username!='admin' and username!='guest'";
		$users = $this->_conn->query($sql);
		//var_dump($result);
		$this->pagination = new Pagination(array(
			'total_items'=>count($users)
		));
		
		$this->main = View::factory('admin/usermanage')
		->set('users',array_slice($users,$this->pagination->offset,$this->pagination->items_per_page));
	}
	
	public function action_columnmanage(){
		//var_dump($this->_session->get('ref'));
		$this->template->navigate[] = '栏目管理';
		
		if ($post = $this->post){
			if (isset($post['name']) && $post['name']){
				$sql = "select id from an_column where name='{$post['name']}'";
				if ($result = $this->_conn->query($sql)){
					Controller_Public::message('栏目名已存在');
					//common::msg('栏目名已存在');
				}else{
					/*list($name,$public) = array($post['name'],$post['public']);
					$sql = "insert into an_column (name,position,public) values('{$name}',1,$public)";*/
					$name = $post['name'];
					$sql = "select max(position) max from an_column";
					$result = $this->_conn->query($sql);
					$position = $result[0]['max'];
					$sql = "insert into an_column (name,position) values('{$name}',{$position}+1)";
					if (false === $this->_conn->query($sql)){
						die($this->_conn->last_error());
					}else{
						logunit::log($this->user->id,'栏目添加',$sql,$name);
					}
					project::redirect('admin/columnmanage');
				}
			}
		}
		
		$columns = $this->columns;
		foreach ($columns as &$column){
			$sql = "select count(id) num from an_resource where columnId=".$column['id'];
			$temp = $this->_conn->query($sql);
			$column['resourceNum'] = $temp[0]['num'];
		}
		
		$this->pagination = new Pagination(array(
			'total_items'=>count($columns)
		));
		
		$this->main = View::factory('admin/columnmanage')
		->set('columns',array_slice($columns,$this->pagination->offset,$this->pagination->items_per_page));
	}
	
	public function action_columnchange(){
		$this->template->navigate[] = '栏目修改';
		
		if ($columnId = $this->request->param('id')){
			if ($post = $this->post){
				/*list($name,$public) = array($post['name'],$post['public']);
				$sql = "update an_column set name='{$name}',public={$public} where id=".$columnId;*/
				//栏目名
				$name = $post['name'];
				$sql = "update an_column set name='{$name}' where id=".$columnId;
				$this->_conn->query($sql);
				/*//会员权限
				if (isset($post['userpower'])){
					//先重置所有该栏目的权限
					$sql = "delete from an_columnpower where columnId={$columnId}";
					$this->_conn->query($sql);
					foreach ($post['userpower'] as $userId=>$level){
						if ('0' != $level){
							$sql = "insert into an_columnpower (userId,columnId,level) values({$userId},{$columnId},{$level})";
							$this->_conn->query($sql);
						}
						
						if (0 < (int)current(current($this->_conn->query("select count(id) from an_columnpower where userId={$userId} and level=1")))){
							$userPower = 2;
						}elseif (0 < (int)current(current($this->_conn->query("select count(id) from an_columnpower where userId={$userId} and level=2")))){
							$userPower = 3;
						}else{
							$userPower = 4;
						}
						//更新会员权限等级
						$sql = "update an_user set power={$userPower} where id=".$userId;
						$this->_conn->query($sql);
					}
				}*/
				
				logunit::log($this->user->id,'栏目修改',$sql,$name);
				Controller_Public::message('栏目已修改');
			}
			
			$sql = "select * from an_column where id={$columnId} limit 1";
			$result = $this->_conn->query($sql);
			$column = $result[0];
			
			$sql = "select * from an_user where deleted=0 and username!='admin' and username!='guest'";
			$users = $this->_conn->query($sql);
			
			$userPower = array();
			$sql = "select * from an_columnpower where columnId={$columnId}";
			foreach ($this->_conn->query($sql) as $power){
				$userPower[$power['userId']] = $power;
			}
		}
		
		$this->main = View::factory('admin/columnchange')
		->set('column',$column)
		->set('users',$users)
		->set('userPower',$userPower);
	}
	
	public function action_resourcemanage(){
		$this->template->navigate[] = '栏目下属资源管理';
		
		$result = array();
		if ($columnId = $this->request->param('id')){
			$sql = "select name from an_column where id={$columnId}";
			$result = $this->_conn->query($sql);
			$columnName = $result[0]['name'];
			$sql = "select r.*,u.username from an_resource r left join an_user u on r.userId=u.id where r.columnId={$columnId} order by r.addTime desc";
			$results = $this->_conn->aliasClean($this->_conn->query($sql));
		}
		
		$this->pagination = new Pagination(array(
			'total_items'=>count($results)
		));
		
		$this->main = View::factory('admin/resourcemanage')
		->set('columnName',$columnName)
		->set('resources',array_slice($results,$this->pagination->offset,$this->pagination->items_per_page));
	}
	
	public function action_resourceview(){
		$this->template->navigate[] = '资源查看/编辑';
		
                if (isset($_COOKIE['html_editor']))
                    $html_editor = $_COOKIE['html_editor'];
                else
                    $html_editor = 'kindeditor';
                
		$resource = null;
		if ($resourceId = $this->request->param('id')){
			if ($post = $this->post){
				list($title,$columnId,$content) = array($post['title'],$post['columnId'],$post['upload_content']);
				$sql = "update an_resource set title='{$title}',columnId={$columnId},content='{$content}' where id={$resourceId}";
				$this->_conn->query($sql);
				logunit::log($this->user->id,'资源编辑',$sql,$title);
				Controller_Public::message('编辑成功');
			}
			$sql = "select * from an_resource where id=".$resourceId." limit 1";
			$result = $this->_conn->query($sql);
			$resource = $result[0];
		}
		
		$sql = "select id,name from an_column order by position";
		$result = $this->_conn->query($sql);
		$columns = array();
		if ($result){
			foreach ($result as $result){
				$columns[$result['id']] = $result['name'];
			}
		}
		
		$this->main = View::factory('admin/resourceview')
		->set('columns',$columns)
		->set('resource',$resource)
                ->set('html_editor',m::factory($html_editor,array('content'=>$resource['content'])));
	}
	
	public function action_resourceupload(){
		$this->template->navigate[] = '资源上传';
		set_time_limit(0);
                
                if (isset($_COOKIE['html_editor']))
                    $html_editor = $_COOKIE['html_editor'];
                else
                    $html_editor = 'kindeditor';
                
		//var_dump($this->post,$_FILES);exit;
		if ($post = $this->post){
			$post = common::sendfix($post,array('recommend'=>'0'));
			$success = true;
			if ($_FILES){
				$content = common::upload('upload_content');
				if (is_array($content)){
					$content = current($content);
					
					/*if ('mov' == $post['type']){
						$content = EncodeFlv::instance($content)->encode();
					}*/
				}else{
					$success = false;
					__404(new lib_exception($content));
					//Controller_Public::message($content);
				}
			}else{
				$content = str_replace('&quot;\&quot;', '&quot;&quot;', addslashes($post['upload_content']));
			}
			
			if ($success){
				list($title,$columnId,$userId,$type,$recommend,$addTime) = array($post['title'],$post['columnId'],$this->user->id,$post['type'],$post['recommend'],time());
				$sql = "insert into an_resource (title,columnId,userId,type,content,recommend,addTime) values('{$title}',{$columnId},{$userId},'{$type}','{$content}',{$recommend},{$addTime})";
				$this->_conn->query($sql);
				logunit::log($this->user->id,'资源上传',$sql,'title=>'.$title);
				Controller_Public::message('<table><tr><td><img src="/media/image/success.gif" /></td><td><b>上传成功</b></td></tr></table>');
			}
		}
		
		$columns = array();
		foreach ($this->columns as $columnId=>$column){
			$columns[$columnId] = $column['name'];
		}
		//var_dump($columns);
		
		$this->main = View::factory('user/resourceupload')
		->set('columns',$columns)
                ->set('html_editor',m::factory($html_editor));
	}
	
	public function action_recommend(){
		$resource = null;
		if (($resourceId = $this->request->param('id')) && isset($_POST['recommend'])){
			$sql = "update an_resource set recommend={$_POST['recommend']} where id={$resourceId}";
			$this->_conn->query($sql);
			logunit::log($this->user->id,'推荐到首页',$sql,$_POST['recommend']);
			echo '1';
		}
	}
	
	public function action_userpower(){
		$this->template->navigate[] = '用户权限设置';
		
		$user = null;
		if ($userId = $this->request->param('id')){
			$sql = "select count(id) from an_user where id={$userId} limit 1";
			if (!$result = $this->_conn->query($sql)){
				die('用户不存在');
			}
			if ($post = $this->post){
				/*list($power,$columns) = array($post['power'],implode(',',$post['columns']));
				$sql = "update an_user set power={$power},columns='{$columns}' where id=".$userId;*/
				//var_dump($post['columnpower']);
				//先重置所有该会员的权限
				$sql = "delete from an_columnpower where userId={$userId}";
				$this->_conn->query($sql);
				/*//设置权限
				$userPower = 4;
				foreach ($post['columnpower'] as $columnId=>$level){
					if ('0' != $level){
						//判权
						if (2 !== $userPower){
							if ('1' == $level){
								$userPower = 2;
							}elseif ('2' == $level){
								$userPower = 3;
							}
						}
						$sql = "insert into an_columnpower (userId,columnId,level) values({$userId},{$columnId},{$level})";
						$this->_conn->query($sql);
					}
				}
				//更新会员权限等级
				$sql = "update an_user set power={$userPower} where id=".$userId;
				$this->_conn->query($sql);*/
				
				//设置权限
				$userPower = 4;
				if (!empty($post['columnpower'])){
					foreach ($post['columnpower'] as $columnId){
						$sql = "insert into an_columnpower (userId,columnId,level) values({$userId},{$columnId},1)";
						$this->_conn->query($sql);
						$userPower = 2;
					}
				}
				//更新会员权限等级
				$sql = "update an_user set power={$userPower} where id=".$userId;
				$this->_conn->query($sql);
				//记录
				logunit::log($this->user->id,'用户权限修改',$sql,$this->_config->userPower[$userPower]);
				Controller_Public::message('用户权限修改成功');
			}
			
			$sql = "select * from an_user where id={$userId} limit 1";
			$result = $this->_conn->query($sql);
			$user = $result[0];
			
			$columnPower = array();
			$sql = "select * from an_columnpower where userId={$userId}";
			foreach ($this->_conn->query($sql) as $power){
				$columnPower[$power['columnId']] = $power;
			}
			//var_dump($columnPower);
		}
		
		$this->main = View::factory('admin/userpower')
		->set('user',$user)
		->set('columnPower',$columnPower)
		->set('columns',$this->columns);
	}
	
	public function action_url(){
		$this->template->navigate[] = '常用网址';
		if ($post = $this->post){
			if (common::preg($post['url'],'url')){
				$sql = "select max(position) max from an_url";
				$result = $this->_conn->query($sql);
				$position = $result[0]['max'];
				list($website,$url,$addTime) = array($post['website'],common::urlpad($post['url']),time());
				$sql = "insert into an_url (website,url,position,addTime) values('{$website}','{$url}',{$position}+1,{$addTime})";
				$this->_conn->query($sql);
				logunit::log($this->user->id,'添加常用网址',$sql,$website);
				Controller_Public::message('常用网址添加成功');
			}else{
				Controller_Public::message('网址格式不正确');
			}
			
		}
		
		$sql = "select * from an_url order by position";
		$urls = $this->_conn->query($sql);
		
		$this->pagination = new Pagination(array(
			'total_items'=>count($urls)
		));
		
		$this->main = View::factory('admin/url')
		->set('urls',array_slice($urls,$this->pagination->offset,$this->pagination->items_per_page));
	}
	
	//访问记录
	public function action_login(){
		$this->template->navigate[] = '访问记录';
		
		$sql = "select u.username,u.power,l.ip,l.addTime from an_login l left join an_user u on l.userId=u.id order by l.addTime desc";
		$logins = $this->_conn->aliasClean($this->_conn->query($sql));
		
		$this->pagination = new Pagination(array(
			'total_items'=>count($logins)
		));
		
		$this->main = View::factory('user/login')
		->set('logins',array_slice($logins,$this->pagination->offset,$this->pagination->items_per_page));
	}
	
	//操作日志 
	public function action_log(){
		$this->template->navigate[] = '操作日志';
		
		$sql = "select u.username,u.power,l.action,l.addTime from an_log l left join an_user u on l.userId=u.id order by l.addTime desc";
		$logs = $this->_conn->aliasClean($this->_conn->query($sql));
		
		$this->pagination = new Pagination(array(
			'total_items'=>count($logs)
		));
		
		$this->main = View::factory('user/log')
		->set('logs',array_slice($logs,$this->pagination->offset,$this->pagination->items_per_page));
	}
	
	//修改密码
	public function action_passwordchange(){
		$this->template->navigate[] = '修改密码';
		
		if ($post = $this->post){
			$sql = "select * from an_user where id={$this->user->id} and deleted!=1";
			$result = $this->_conn->query($sql);
			list($previousPass,$currentPass) = array(md5($post['previous']),md5($post['current']));
			if ($result && $previousPass === $result[0]['password']){//有该账户并且密码正确
				$sql = "update an_user set password='".$currentPass."' where id=".$this->user->id;
				$this->_conn->query($sql);
				Controller_Public::message('密码修改成功');
			}else{
				Controller_Public::message('信息错误');
			}
		}
		
		$this->main = View::factory('user/passwordchange');
	}
	
	//删除操作日志
	public function action_dellog(){
		if ($this->post['before']){
			$before = strtotime($this->post['before']);
			$sql = "select id from an_log where addTime<".$before;
			$num = $this->_conn->numRows($sql);
			
			$sql = "delete from an_log where addTime<".$before;
			$this->_conn->query($sql);
			Controller_Public::message('删除成功,共删除了'.$num.'条记录');
		}
		$this->request->redirect($this->_session->get('ref')->HTTP_REFERER);
	}

	//删除访问记录
	public function action_dellogin(){
		if ($this->post['before']){
			$before = strtotime($this->post['before']);
			$sql = "select id from an_login where addTime<".$before;
			$num = $this->_conn->numRows($sql);
			
			$sql = "delete from an_login where addTime<".$before;
			$this->_conn->query($sql);
			Controller_Public::message('删除成功,共删除了'.$num.'条记录');
		}
		$this->request->redirect($this->_session->get('ref')->HTTP_REFERER);
	}
} // End Welcome
