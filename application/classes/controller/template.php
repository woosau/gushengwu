<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Template extends Controller {
	
	//模板文件
	protected $template = 'template';
	
	//模版内容
	protected $html = array('head'=>true,'foot'=>true);
	
	//是否ajax
	public $_is_ajax;
	
	//数据库实体
	public $_conn;
	
	//服务器端缓存
	public $_session;
	
	//文件缓存
	public $_cache;
	
	//配置容器
	public $_config;
	
	//分页容器
	public $pagination;
	
	//是否自动显示
	protected $auto_render = TRUE;
	
	//用户变量
	public $user;
	
	//$_POST数组
	public $post;
	
	//$_GET数组
	public $get;
	
	public function before(){
		parent::before();
		
		include_once Kohana::find_file('classes','common');
		
		$this->_conn = SqliteDB::instance(Kohana::$config->load('conndb')->dbname);
		
		$this->_session = Session::instance();
        
        $this->_cache = Cache::instance();
        
        $this->_config = Kohana::$config->load('website');
        
        $this->post = lib_security::killinject($this->request->post());
        
        $this->get = lib_security::killinject($_GET);
        
        /*$ip = getip();
        $preg = "/(159\.226\.74\.|119\.78\.212\.){1}\d+(\:24)?/";
        logunit::log(1,'IP进入','preg=>'.preg_match($preg,$ip),$ip);*/
        
        if (!$backupTimeup = $this->_cache->get('backupTimeup')){
        	common::fileBackup(Kohana::$config->load('conndb')->dbname);
        	$this->_cache->set('backupTimeup',true,Kohana::$config->load('conndb')->backupInterval);
        }
        
        if (!$this->user = $this->_session->get('user')){
        	$user = (object)array ('id' => 0, 'username' => 'guest','power'=>0,'powerName'=>'游客', 'ip' => common::getip() );
			$this->_session->set ( 'user', $user );
			$this->user = $user;
        }
        
        //首页可以看到的栏目
		//$this->columns = $this->columnIds = array();
		/*if (0 == $this->user->id) { //如果是是游客
			$sql = "select * from an_column where public=1 order by position";
		} else{
			$sql = "select power,columns from an_user where id=".$this->user->id;
			$user = current($this->_conn->query($sql));
			if (4 == $user ['power']) { //如果是普通用户
				$sql = "select power,columns from an_user where id=" . $this->user->id;
				$user = current ( $this->_conn->query ( $sql ) );
				$sql = "select * from (select * from an_column where id in ({$user['columns']}) union select * from an_column where public=1) order by position";
			} else {
				$sql = "select * from an_column order by position";
			}
		}*/
        $this->columns = array();
		$sql = "select * from an_column order by position";
		foreach ($this->_conn->query ( $sql ) as $column){
			$this->columns[$column['id']] = $column;
		}
		/*foreach ($this->columns as $column){
			$this->columnIds[] = $column['id'];
		}*/
		
		//可以管理的栏目,'1'=>'子栏目管理员','2'=>'子栏目助手'
		$this->columnPower = array(1=>array(),2=>array());
		$sql = "select * from an_columnpower where userId=".$this->user->id;
		foreach ($this->_conn->query ( $sql ) as $power){
			if ('2' == $power['level']){
				$this->columnPower[2][] = $power['columnId'];
			}elseif ('1' == $power['level']){
				$this->columnPower[1][] = $this->columnPower[2][] = $power['columnId'];
			}
		}
        
		if ($this->request->is_ajax())
		{
			$this->_is_ajax = TRUE;
		}
		
        if ($this->_is_ajax === TRUE)
		{
			// Turn off auto-rendering
			$this->auto_render = FALSE;
			
			// Send headers for a json response
			$this->request->headers ['Cache-Control'] = 'no-cache, must-revalidate';
			$this->request->headers ['Expires'] = 'Sun, 30 Jul 1989 19:30:00 GMT';
		}
		
		//初始化模板视图
		$this->template = View::factory($this->template)
		->set('object',$this);
		
		if (TRUE === $this->auto_render){
			//前一页缓存
			if (isset ( $_SERVER ['HTTP_REFERER'] ) && isset ( $_SERVER ['REDIRECT_URL'] )) {
				$HTTP_REFERER = parse_url ( $_SERVER ['HTTP_REFERER'] );
				$paramId = $this->request->param ( 'id' );
				if (! $this->_session->get ( 'ref' ) || trim ( $HTTP_REFERER ['path'], $paramId . '/' ) != trim ( $_SERVER ['REDIRECT_URL'], $paramId . '/' )) {
					$this->_session->set ( 'ref', ( object ) array (
						'REDIRECT_URL' => $_SERVER ['REDIRECT_URL'],
						'HTTP_REFERER' => "{$_SERVER['HTTP_REFERER']}"
					) );
				}
			} else {
				$this->_session->set ( 'ref', ( object ) array (
					'REDIRECT_URL' => '',
					'HTTP_REFERER' => ''
				) );
			}
			//标题
            $this->template->title = '首页';
            
            //导航
            $this->template->navigate = array('首页');
            
            //样式表
            $this->template->styles = array_merge(array(), array(
            	'media/css/website.css',
            	'media/css/jquery-ui-1.8.16.custom.css'
            ));
            
            //js
            $this->template->scripts = array_merge(array(), array(
            	'media/js/jquery-1.7.1.min.js',
            	'media/js/jquery-ui-1.8.16.custom.min.js',
                'media/js/datepicker/WdatePicker.js',
            	'media/js/website.js',
            	'media/js/cookie.js',
            ));
		}
	}
	
	public function after(){
		if (TRUE === $this->auto_render){
			if ($this->html['head']){
				$head = View::factory('common/head')
				->set('object',$this);
			}else{
				$head = '';
			}
			if ($this->html['foot']){
				$foot = View::factory('common/foot')
				->set('object',$this);
			}else{
				$foot = '';
			}
			$this->template
			->set('extra',View::factory('common/extra'))
			->set('head',$head)
			->set('foot',$foot);
			$this->response->body($this->template->render());
		}
		
		parent::after();
	}
	
	public function action_index()
	{
		exit;
	}

} // End Welcome
