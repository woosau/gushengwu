<?php defined('SYSPATH') or die('No direct access allowed.');
class lib_exception extends Exception{
    
    private $error_message = 'sorry,you meet an unknown error';
    private $error_code = 500;
    private $error_file = '';
    private $error_line = 0;
    private $error_url = '';
    
    private $_404_controller = '404';
    
    public function __construct($message = null,$code = null){
        if (is_string($message)) $this->error_message = $message;
        if (is_int($code)) $this->error_code = $code;
        $this->error_file = basename($this->getFile());
        $this->error_line = $this->getLine();
        $this->error_url = (string)URL::base(true).Request::current()->uri().URL::query();
        parent::__construct($this->error_message,$this->error_code);
    }
    
    public static function instance($message = null,$code = null){
        return new self($message,$code);
    }
    
    public function getArray($toJson = false){
        $error_array = array(
            'message'=>$this->getMessage(),
            'code'=>$this->getCode()
        );
        if ($toJson) return json_encode($error_array);
        return $error_array;
    }
    
    public function log($errorinf = 'error',$userId = 0){
        if (0 == $userId){
	        $user = Session::instance()->get('user');
	        if (is_object($user) && isset($user->id)){
	            $userId = $user->id;
	        }else{
	            $userId = 1;
	        }
        }
        $content = 'message=>'.$this->error_message
        .',code=>'.$this->error_code
        .',line=>'.$this->error_line;
        logunit::log(intval($userId),(string)$errorinf,false,$content);
        return $this;
    }
    
    public function __404(){
        //Kohana_Request::current()->redirect('404');
        header('HTTP/1.0 404 Not Found');
        if ($this->_404_controller){
        	$view = Request::factory($this->_404_controller)->post(array(
	            'url'=>$this->error_url,
	        	'message'=>$this->error_message,
	            'code'=>$this->error_code,
	            'file'=>$this->error_file,
	            'line'=>$this->error_line,
	            'trace'=>$this->getTrace(),
	            'traceAsString'=>$this->getTraceAsString()
	        ))->execute();
        }else{
        	$view = $this->error_message;
        }
        echo Request::current()->create_response()->body($view);
        exit;
    }
    
    public function __call($name,$arguments){
        $this->__404();
    }
}