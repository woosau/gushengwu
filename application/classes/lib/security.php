<?php
class lib_security{
	public static function killinject($post){
		if (is_string($post)){
			$post = array($post);
		}
		array_walk_recursive($post,array('self','DOkillinject'));
		return $post;
	}
	
	static function DOkillinject($value, $key){
		$inject = false;
		if (false !== strpos($value,'select')){
			$inject = true;
		}
		if (false !== strpos($value,'update')){
			$inject = true;
		}
		if (false !== strpos($value,'insert')){
			$inject = true;
		}
		if (false !== strpos($value,'delete')){
			$inject = true;
		}
		if ($inject) die('未知错误，系统运行中止');
	}
}