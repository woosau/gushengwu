<?php
class EncodeFlv{
	
	private static $instance;
	
	private $config;
	
	private $ffmpeg,$mdi;
	
	private $strResult = array(),$runningResult = array();
	
	private $from,$to;
	
	private $cmd = array();
	
	public static function stop($warning){
		header("content-type:text/html;charset=utf-8");
		die($warning);
	}
	
	public static function instance($from = '',$to = ''){
		if (!function_exists('exec')){
			self::stop('视频转换需求函数被禁用');
		}
		if (!$from){
			self::stop('未提供来源文件');
		}elseif(!is_file($from)){
			self::stop('无效来源文件');
		}
		if (!$to){
			$filedir = dirname($from);
			$filename = basename($from,'.'.pathinfo($from,PATHINFO_EXTENSION));
			$to = $filedir.DIRECTORY_SEPARATOR.$filename.'.flv';
		}elseif('flv' != pathinfo($to,PATHINFO_EXTENSION)){
			$to .= '.flv';
		}
		if(is_file($to)){
			self::stop('目标同名文件已存在');
		}
		
		self::$instance = new self;
		list(self::$instance->from,self::$instance->to) = array($from,$to);
		return self::$instance;
	}
	
	public function config(array $config){
		//var_dump($config);
		//echo is_file($config['ffmpeg']);
		if (isset($config['ffmpeg']) && is_file($config['ffmpeg'])){
			$this->ffmpeg = $config['ffmpeg'];
		}else{
			self::stop('请传入正确的ffmpeg.exe工具路径');
		}
		if (isset($config['mdi']) && is_file($config['mdi'])){
			$this->mdi = $config['mdi'];
		}else{
			self::stop('请传入正确的mdi.exe工具路径');
		}
		$this->config = $config;
		return $this;
	}
	
	public function encode($del = true){
		set_time_limit(0);
		
		if (!is_array($this->config)){
			$config = array(
				'ffmpeg'=>'exec'.DIRECTORY_SEPARATOR.'ffmpeg.exe',
				'mdi'=>'exec'.DIRECTORY_SEPARATOR.'flvmdi.exe'
			);
			$this->config($config);
		}
		
		$this->cmd['ffmpeg'] = $this->ffmpeg.' -i '.$this->from.' '.$this->to;
		$this->cmd['mdi'] = $this->mdi.' '.$this->to;
		
		$strResult[] = exec($this->cmd['ffmpeg'],$runningResul[]);
		if (!is_file($this->to)){
			self::stop(var_dump($strResult,$runningResul));
		}
		$strResult[] = exec($this->cmd['mdi'],$runningResul[]);
		
		if ($del){
			@unlink($this->from);
		}
		
		return $this->to;
	}
} 
?>