<?php
class logunit{
	
	private static $tableName = 'an_log';
    
    private static $instance;

    /**
     * 单实例方法
     *
     */
    public static function instance()
    {
        if (!is_object(self::$instance))
        {
            self::$instance = new self;
        }
        return self::$instance;
    }
    
    public static function log($userId = 1,$action = '',$sql = '',$content = '',$dbname = '')
    {
    	$dbname = $dbname?$dbname:Kohana::$config->load('conndb')->dbname;
    	$conn = SqliteDB::instance($dbname);
    	$time = time();
    	if ($sql){
    		$sql = str_replace(array("\n","\r","\""),'',$sql);
    		$sql = addslashes(stripslashes($sql));
    	}
        $queryStr = "insert into ".self::$tableName." (userId,action,content,sql,addTime) values({$userId},'{$action}','{$content}',\"{$sql}\",{$time})";
        $conn->query($queryStr);
        if ($lastInsertRowid = $conn->lastInsertRowid()){
        	return $lastInsertRowid;
        }
        $conn->close();
        return true;
    }
}