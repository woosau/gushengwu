<?php
//第三方类库
class m{
	
	public static function factory($m,$var = null){
		if (is_string($m) && strlen($m)){
			$file = DOCROOT.'M'.DIRECTORY_SEPARATOR.$m.EXT;
			if (is_file($file)) return include $file;
		} 
		
		return new lib_void();
	}
}