<?php
class common
{
	//程序中断，输出错误信息
	public static function stop($warning){
		header("content-type:text/html;charset=utf-8");
		die($warning);
	}
	
	//传递表单数组处理补全
	public static function sendfix($data = array(),$fixer = array()){
		if (empty($data)){
			if ($_POST){
				$data = $_POST;
			}elseif ($_GET){
				$data = $_GET;
			}else{
				return false;
			}
		}
		$data = array_merge($fixer,$data);
		$return = array();
		foreach ($data as $k=>$v){
			if (is_string($v)){
				$return[$k] = addslashes(trim($v));
			}else{
				$return[$k] = $v;
			}
		}
		return $return;
	}
	
	//文件备份，名字用日期时间区别
	public static function fileBackup($file){
		$date = date('Y-m-d');
		$dir = dirname($file);
		if (!is_writable($dir)){
			return false;
		}
		$filename = self::fileprename($file);
		$fileext = self::fileext($file);
		$dest = $dir.'/'.$filename.$date.'.'.$fileext;
		if (!copy($file,$dest)){
			return copy($file,$dest);
		}
		return false;
	}
	
	//字符串格式验证
	public static function preg($str,$type = null){
		switch ($type){
			case 'url':
				$format = '/^http[s]?:\\/\\/([\\w-]+\\.)+[\\w-]+([\\w-.\/?%&=]*)?$/i';
				break;
			case 'email':
				$format = '/^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$/';
				break;
			default:return false;
		}
		return (bool)preg_match($format,$str);
	}
	
	//文件地址转换成file路由
	public static function fileroute($path){
		return 'file/'.basename($path);
	}
	
	//文件或者文件夹附加目录分隔符抬头
	public static function pathpad($path,$root = true){
		$path = ltrim($path,'/\ ');
		if ($root) $path = '/'.$path;
		return $path;
	}
	
	//网址附加http抬头
	public static function urlpad($url,$slash = false){
        $url = (0 === stripos($url,'http')?trim($url,' /'):'http://'.trim($url,' /'));
        if ($slash) $url .= '/';
        return $url;
    }
    
	public static function msg($msg = '',$url = ''){
		$msg = $msg?$msg:'系统出错！请重试';
		echo '<script>alert("'.$msg.'");</script>';
		if ($url){
			echo '<script>location.href="'.$url.'";</script>'; 
		}
	}
	
	//获取ip地址
	public static function getip(){
		/*return $ip = !empty($_SERVER["HTTP_X_FORWARDED_FOR"])
		?$_SERVER["HTTP_X_FORWARDED_FOR"]
		:$_SERVER["REMOTE_ADDR"];*/
		if(getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), 'unknown')) {
            $onlineip = getenv('HTTP_CLIENT_IP');
        } elseif(getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), 'unknown')) {
            $onlineip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif(getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'), 'unknown')) {
            $onlineip = getenv('REMOTE_ADDR');
        } elseif(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown')) {
            $onlineip = $_SERVER['REMOTE_ADDR'];
        }
        $onlineip = addslashes($onlineip);
        @preg_match("/[\d\.]{7,15}/", $onlineip, $onlineipmatches);
        $onlineip = $onlineipmatches[0];
        return $onlineip;
	}

	//上传文件上层处理
    public static function upload($name,$strExt = '',$dateDir = false)
    {
        if (is_array($_FILES[$name]['tmp_name'])){
            $files = array();
            foreach ($_FILES[$name]['tmp_name'] as $k=>$tmp_name){
                if (!$tmp_name) continue;
                $fileinfo = array(
                	'name'=>$_FILES[$name]['name'][$k],
                    'type'=>$_FILES[$name]['type'][$k],
                    'tmp_name'=>$_FILES[$name]['tmp_name'][$k],
                    'error'=>$_FILES[$name]['error'][$k],
                    'size'=>$_FILES[$name]['size'][$k]
                );
                if (is_file($file = self::uploadhanding($fileinfo,$strExt,$dateDir))){
                	$files[$_FILES[$name]['name'][$k]] = $file;
                }
            }
        }else{
        	if (is_file($file = self::uploadhanding($_FILES[$name],$strExt,$dateDir))){
        		$files = array($_FILES[$name]['name']=>$file);
        	}
        }
        return $files;
    }
    
    //上传文件底层处理
    private static function uploadhanding($fileinfo,$strExt = '',$dateDir = false){
        $dir = 'uploadFile'; //上传目录
        if (! is_uploaded_file($fileinfo['tmp_name']))
        {
            return '上传失败';
        }
        $ext = strtolower(common::fileext($fileinfo['name']));
        if ($strExt)
        {
            if (is_string($strExt) && ($strExt != $ext)){
                return '错误的文件扩展名';
            }
            if (is_array($strExt) && !in_array($ext,$strExt)){
                return '错误的文件扩展名';
            }
        }
        if (! is_dir($dir))
        {
            if (! mkdir($dir))
            {
                return '创建上传文件目录失败';
            }
        }elseif (!is_writable($dir)){
            return '上传文件目录不可写';
        }elseif($dateDir){
        	$dir .= DIRECTORY_SEPARATOR.date('Y-m-d');
	        if (! is_dir($dir))
	        {
	            if (! mkdir($dir))
	            {
	                return '创建上传文件日期目录失败';
	            }elseif (!is_writable($dir)){
		            return '上传文件日期目录不可写';
		        }
	        }
        }
        $filename = self::randfilename() . '.' . $ext;
        if (move_uploaded_file($fileinfo['tmp_name'], $file = $dir . '/' . $filename))
        {
            return $file;//最终传递文件全路径
        }else
        {
            return '上传文件转移失败';
        }
        return false;
    }
	
    //获取不带扩展名的文件名
    public static function fileprename($filename){
    	return basename($filename,'.'.pathinfo($filename,PATHINFO_EXTENSION));
    }
    
    //获取文件扩展名
    public static function fileext($filename){
        return pathinfo($filename,PATHINFO_EXTENSION);
    }
    
    //判断输入的时间是否为正确日期格式
	public static function isdate($str,$format="Y-m-d"){
		$strArr = explode("-",$str);
		if(empty($strArr)){
			return false;
		}
		foreach($strArr as $val){
			if(strlen($val)<2){
				$val="0".$val;
			}
			$newArr[]=$val;
		}
		$str =implode("-",$newArr);
		$unixTime=strtotime($str);
		$checkDate= date($format,$unixTime);
		if($checkDate==$str)
			return true;
		else
			return false;
	}
		
	//产生随机字符串
	public static function randStr($len = 6, $format = 'ALL') {
		switch ($format) {
			case 'ALL' :
				$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
				break;
			case 'CHAR' :
				$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
				break;
			case 'NUMBER' :
				$chars = '0123456789';
				break;
			default :
				$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
				break;
		}
		$string = "";
		while ( strlen ( $string ) < $len )
			$string .= substr ( $chars, (mt_rand () % strlen ( $chars )), 1 );
		return $string;
	}
	
    //返回随机文件名
    public static function randfilename($length = 10)
    {
        $hash = '';
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        $max = strlen($chars) - 1;
        //mt_srand((double) microtime() * 1000000);
        for($i = 0;$i < $length;$i ++)
        {
            $hash .= $chars[mt_rand(0, $max)];
        }
        return $hash;
    }
    
    //CURL数据传输
    public static function curl($url,array $params = array()){
        if (!$url){
            return '';
        }
        $init = curl_init();
        curl_setopt($init,CURLOPT_URL,$url);
        curl_setopt($init,CURLOPT_RETURNTRANSFER,true);
        curl_setopt ( $init, CURLOPT_SSL_VERIFYPEER, 0 );
        curl_setopt ( $init, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2 (.NET CLR 3.5.30729)" );
	    //curl_setopt ( $init, CURLOPT_HEADER, false );
	    //curl_setopt ( $init, CURLOPT_NOBODY, false );
	    curl_setopt ( $init, CURLOPT_FOLLOWLOCATION, 1 ); // 使用自动跳转
        if (count($params)){
            curl_setopt($init,CURLOPT_POST,true);
            curl_setopt($init,CURLOPT_POSTFIELDS,$params);
        }
        $result = curl_exec($init);
        if ($error = curl_errno ( $init )) {
		    //echo 'curl error:' . $error;
		    //return 'ERROR';
		    return '';
	    }
        curl_close($init);
        return $result;
    }
    
     //截取一定长度的字符串，确保截取后字符串不出乱码
    function cutstr($str,$cutleng)
	    {
	    $str = $str; //要截取的字符串
	    $cutleng = $cutleng; //要截取的长度
	    $strleng = strlen($str); //字符串长度
	    if($cutleng>$strleng)return $str;//字符串长度小于规定字数时,返回字符串本身
	    $notchinanum = 0; //初始不是汉字的字符数
	    for($i=0;$i<$cutleng;$i++)
	    {
		    if(ord(substr($str,$i,1))<=128)
		    {
			    $notchinanum++;
		    }
	    }
	    if(($cutleng%2==1)&&($notchinanum%2==0))//如果要截取奇数个字符，所要截取长度范围内的字符必须含奇数个非汉字，否则截取的长度加一
	    {
		    $cutleng++;
	    }
	    if(($cutleng%2==0)&&($notchinanum%2==1))//如果要截取偶数个字符，所要截取长度范围内的字符必须含偶数个非汉字，否则截取的长度加一
	    {
		    $cutleng++;
	    }
	    return substr($str,0,$cutleng);
	    }
}

if (!function_exists('__404')){//404异常处理用户函数
    function __404($e,$log = false){
        if (!is_object($e) || !($e instanceof lib_exception)){
            call_user_func(__FUNCTION__,new lib_exception('传入的参数不是异常类对象'));
        }
        if (true === $log){
            $e->log($e->getMessage());
        }elseif (is_string($log)){
            $e->log($log);
        }
        $e->__404();
        exit;
    }
}