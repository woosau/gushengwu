<?php 
$length = isset($_COOKIE['length'])?$_COOKIE['length']:20;
if ($_POST && isset($_POST['length'])){
    setcookie('length',$_POST['length']);
    $length = $_POST['length'];
}?>
<form action="" method="post" id="paginationForm">
<p class="pagination">
每页<?php echo Form::select('length',array('10'=>10,'20'=>20,'40'=>40,'80'=>80),$length)?>项
&nbsp;&nbsp; 
<span style="font-weight:bold;">第&nbsp;<?php echo $current_first_item?>&nbsp;到&nbsp;<?php echo $current_last_item?>&nbsp;项&nbsp;|&nbsp;总共&nbsp;<?php echo $total_items?>&nbsp;项</span>
&nbsp;&nbsp; 
	<?php if ($first_page !== FALSE): ?>
		<a href="<?php echo $page->url($first_page) ?>" rel="first">首页</a>
	<?php else: ?>
		<span style="color:#CCCCCC;display:inline-block;padding:3px">首页</span>
	<?php endif ?>

	<?php if ($previous_page !== FALSE): ?>
		<a href="<?php echo $page->url($previous_page) ?>" rel="prev">前一页</a>
	<?php else: ?>
		<span style="color:#CCCCCC;display:inline-block;padding:3px">前一页</span>
	<?php endif ?>

	<?php for ($i = 1; $i <= $total_pages; $i++): ?>

		<?php if ($i == $current_page): ?>
			<span style="color:#CCCCCC;display:inline-block;padding:3px"><?php echo $i ?></span>
		<?php else: ?>
			<a href="<?php echo $page->url($i) ?>"><?php echo $i ?></a>
		<?php endif ?>

	<?php endfor ?>

	<?php if ($next_page !== FALSE): ?>
		<a href="<?php echo $page->url($next_page) ?>" rel="next">后一页</a>
	<?php else: ?>
		<span style="color:#CCCCCC;display:inline-block;padding:3px">后一页</span>
	<?php endif ?>

	<?php if ($last_page !== FALSE): ?>
		<a href="<?php echo $page->url($last_page) ?>" rel="last">尾页</a>
	<?php else: ?>
		<span style="color:#CCCCCC;display:inline-block;padding:3px">尾页</span>
	<?php endif ?>

</p><!-- .pagination -->
</form>
<script type="text/javascript">
<!--
$('select[name=length]').change(function(){
	$('#paginationForm').submit();
});
//-->
</script>