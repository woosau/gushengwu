<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title>用户登录</title>
<link href="/media/css/login.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/media/js/jquery-1.7.1.min.js"></script>
<script>
function loginformsubmit(action){
	if (!document.getElementById('usernameText').value){
		alert('请填写用户名');
		return;
	}
	if (!document.getElementById('passwordText').value){
		alert('请填写密码');
		return;
	}
	if (!document.getElementById('verifycodeText').value){
		alert('请填写验证码');
		return;
	}
	document.getElementById('loginform').setAttribute('action','?action='+action);
	document.getElementById('loginform').submit();
}
</script>
<script>
$(function(){
	element = $('script[src^=http]').parents('div').remove();
})
</script>
</head>
<body>

    <div id="login">
	
	     <div id="top">
		      <div id="top_left"><img src="/media/image/login/login_03.gif" /></div>
			  <div id="top_center"></div>
		 </div>
		 
		 <div id="center">
		      <div id="center_left"></div>
			  <div id="center_middle">
			  <form action="" method="post" id="loginform">
			       <div id="user">用&nbsp;户
			         <input id="usernameText" type="text" name="username" value="<?php echo $username;?>" />
			       </div>
				   <div id="password">密&nbsp;码
				     <input id="passwordText" type="password" name="password" />
				   </div>
				   <div id="user">验&nbsp;证
				   	<input id="verifycodeText" type="text" name="verifycode" style="width:50px;" />
				   	<img src='public/verifycode' id="verifycode" title="看不清楚?点击切换!" onclick="this.src='';this.src='public/verifycode';"/>
			       </div>
			       <div id="message"><?php echo $message?>
				   </div>
				   <div id="btn">
				   <a href="javascript:loginformsubmit('login')">登录</a>
				   <!--<a onclick="return confirm('用所填写的用户名和密码注册？')" href="javascript:loginformsubmit('register')">注册</a>-->
				   <a href="javascript:document.getElementById('loginform').reset()">重填</a>
				   </div>
			  </form>
			  </div>
			  <div id="center_right"></div>		 
		 </div>
		 <div id="down">
		      <div id="down_left">
			      <div id="inf">
                       <span class="inf_text">版本信息</span>
					   <span class="copyright">所内后台管理系统</span>
			      </div>
			  </div>
			  <div id="down_center"></div>		 
		 </div>

	</div>
</body>
</html>
