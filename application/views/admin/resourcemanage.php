<script type="text/javascript">
function recommend(obj){
	$.ajax({
		type:'post',
		url:'admin/recommend/'+$(obj).val(),
		data:'recommend='+(obj.checked?'1':'0'),
		async:false,
		success:function(msg){
			if ('1'!==msg){
				alert(msg);
			}
		}
	});
}
</script>
<h3>栏目名：<?php echo $columnName?></h3>
<?php echo $pagination?>
<table cellpadding="0" cellspacing="1" bgcolor="#c9c9c9">
	<tr>
		<td><input type="checkbox" onclick="checkall(this)" /></td>
		<th class="STYLE1">序号</th>
		<th class="STYLE1">资源标题</th>
		<th class="STYLE1">上传用户</th>
		<th class="STYLE1">资源类型</th>
		<th class="STYLE1">资源内容</th>
		<th class="STYLE1">上传时间</th>
		<th class="STYLE1">推荐</th>
		<th class="STYLE1">操作</th>
	</tr>
<?php foreach ($resources as $k=>$resource):?>
	<tr>
		<td><input type="checkbox" value="<?php echo $resource['id']?>" /></td>
		<td class="STYLE3"><?php echo $k+1?></td>
		<td class="STYLE3"><?php echo $resource['title']?></td>
		<td class="STYLE3"><?php echo $resource['username']?></td>
		<td class="STYLE3"><?php echo $resource['type']?></td>
		<td class="STYLE3"><?php echo $resource['content']?></td>
		<td timestamp="timestamp" class="STYLE3"><?php echo date('Y-m-d',$resource['addTime'])?></td>
		<td class="STYLE3"><?php echo Form::checkbox(false,$resource['id'],(bool)$resource['recommend'],array('onclick'=>'recommend(this)'))?></td>
		<td class="STYLE3"><a href="admin/resourceview/<?php echo $resource['id']?>">资源查看</a> <a onclick="return confirm('确定删除该资源？')" href="private/action/resource_del_<?php echo $resource['id']?>">删除</a></td>
	</tr>
<?php endforeach;?>
</table>