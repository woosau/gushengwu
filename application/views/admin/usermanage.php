<script type="text/javascript">
<!--
function useradd(){
	if(!document.getElementById('username').value){
		alert('请填写用户名');
		return false;
	}
	return confirm('确认增加？');
}
//-->
</script>

<table cellpadding="0" cellspacing="0">
	<tr>
		<td>
		<form action="" method="post">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td>
				<img src="/media/image/default/001.gif" />
				</td>
				<td>用户名：<input type="text" id="username" name="username" /></td>
				<td class="spanclick STYLE1"><input type="submit" value="新增" onclick="return useradd()"/></td>
			</tr>
			<tr>
				<td colspan="0">
				*新增用户默认密码:<?php echo Kohana::$config->load('conndb')->defaultUserPassword?>
				</td>
			</tr>
		</table>
		</form>
		</td>
	</tr>
</table>

<?php echo $pagination?>
<table border="0" cellpadding="0" cellspacing="1" bgcolor="#c9c9c9">
	<tr>
		<td><input type="checkbox" onclick="checkall(this)" /></td>
		<th class="STYLE1">序号</th>
		<th class="STYLE1">用户名</th>
		<th class="STYLE1">权限</th>
		<th class="STYLE1">注册IP</th>
		<th class="STYLE1">注册时间</th>
		<th class="STYLE1">操作</th>
	</tr>
<?php foreach ($users as $k=>$user):?>
	<tr>
		<td><input type="checkbox" value="<?php echo $user['id']?>" /></td>
		<td class="STYLE3"><?php echo $k+1?></td>
		<td class="STYLE3"><?php echo $user['username']?></td>
		<td class="STYLE3"><?php echo $object->_config->userPower[$user['power']]?></td>
		<td class="STYLE3"><?php echo $user['ip']?></td>
		<td timestamp="timestamp" class="STYLE3"><?php echo date('Y-m-d',$user['addTime'])?></td>
		<td class="STYLE3">
		<a href="admin/userpower/<?php echo $user['id']?>">权限设置</a>
		<a onclick="return confirm('确定删除该用户？')" href="private/action/user_del_<?php echo $user['id']?>">删除</a>
		</td>
	</tr>
<?php endforeach;?>
</table>
