<script>
function change_editor(obj){
    editor = $(obj).val();
    cookie.prototype.set('html_editor',editor);
    location.reload();
}
</script>

<form action="" method="post" enctype="multipart/form-data">
<table cellpadding="0" cellspacing="1" bgcolor="#c9c9c9">
	<tr>
		<td></td>
        <td><input type="submit" value="提交" /></td>
    </tr>
    <tr>
        <td>选择html编辑器：</td>
        <td><?php echo Form::select(null,array("kindeditor"=>'kindeditor',"xheditor"=>'xheditor',"ckeditor"=>'ckeditor',"tinymce"=>'tinymce'),isset($_COOKIE['html_editor'])?$_COOKIE['html_editor']:null,array('onchange'=>'change_editor(this)'))?></td>
    </tr>
    <tr>
        <td>标题：</td>
        <td><?php echo Form::input('title',$resource['title'])?></td>
    </tr>
    <tr>
        <td>栏目：</td>
        <td><?php echo Form::select('columnId',$object->_config->noselect+$columns,$resource['columnId'])?></td>
    </tr>
    <tr>
        <td>类型：</td>
        <td><?php echo $object->_config->resourceType[$resource['type']]?></td>
    </tr>
    <tr>
        <td>内容：</td>
        <td id="content">
        
        <?php if ('art' == $resource['type']):?>
        <?php echo $html_editor?>
        <?php else:?>
        <a href="<?php echo common::fileroute($resource['content'])?>">点击打开文件</a>
        <input type="hidden" value="<?php echo $resource['content']?>" name="upload_content" />
        <?php endif;?>
        
        </td>
    </tr>
    <tr>
        <td></td>
        <td><input type="submit" value="提交" /></td>
    </tr>
</table>
</form>