<!--header==begin-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title?><?php echo $object->_config->title;?></title>
<base href="<?php echo $object->_config->host;?>" />
<?php foreach ($styles as $style) echo HTML::style($style, NULL, TRUE), "\n" ?>
<?php foreach ($scripts as $script) echo HTML::script($script, NULL, TRUE), "\n" ?>
</head>

<body>

<?php echo $extra;?>

<?php echo $head;?>
<?php echo $content;?>
<?php echo $foot;?>

<!--[if IE 6]>
<script type="text/javascript" src="http://ued.bizark.com/pro/platform2b/js/ie6.js"></script>
<![endif]-->

<!--bottom==begin-->
</body>
</html>
<!--bottom==end-->