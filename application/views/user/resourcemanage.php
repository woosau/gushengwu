<h3>栏目名：<?php echo $columnName?></h3>
<h3>当前权限：<?php echo $object->_config->columnPower[$level]?></h3>
<table cellpadding="0" cellspacing="1" bgcolor="#c9c9c9">
	<tr>
		<td><input type="checkbox" onclick="checkall(this)" /></td>
		<th class="STYLE1">序号</th>
		<th class="STYLE1">资源标题</th>
		<th class="STYLE1">上传用户</th>
		<th class="STYLE1">资源类型</th>
		<th class="STYLE1">资源内容</th>
		<th class="STYLE1">上传时间</th>
		<th class="STYLE1">操作</th>
	</tr>
<?php foreach ($resources as $k=>$resource):?>
	<tr>
		<td><input type="checkbox" value="<?php echo $resource['id']?>" /></td>
		<td class="STYLE3"><?php echo $k+1?></td>
		<td class="STYLE3"><?php echo $resource['title']?></td>
		<td class="STYLE3"><?php echo $resource['username']?></td>
		<td class="STYLE3"><?php echo $resource['type']?></td>
		<td class="STYLE3"><?php echo $resource['content']?></td>
		<td timestamp="timestamp" class="STYLE3"><?php echo date('Y-m-d',$resource['addTime'])?></td>
		<td class="STYLE3">
		<a href="user/resourceview/<?php echo $resource['id']?>">资源查看</a>
		<?php if (1 == $level || $resource['userId'] == $object->user->id):?>
		<a onclick="return confirm('确定删除该资源？')" href="private/action/resource_del_<?php echo $resource['id']?>">删除</a>
		<?php endif;?>
		</td>
	</tr>
<?php endforeach;?>
</table>