<script>
function uploadForm(){
	if (!$('input[name=title]').val()){
		alert('请输入标题');
		return false;
	}
	if (!$('select[name=columnId]').val()){
		alert('请选择要上传目标栏目');
		return false;
	}
//	if ('art' == $('select[name=type]').val() && !$('textarea[name=content]').val()){
//		alert('请填写文章内容');
//		return false;
//	}
	return true;
}
function change_editor(obj){
    editor = $(obj).val();
    cookie.prototype.set('html_editor',editor);
    location.reload();
}
$(function(){
	$('select[name=type]').change(function(){
		$('#content div.content').hide();
		$('[name=upload_content]').attr('disabled','disabled');
		$('div#'+this.value).show().find('[name=upload_content]').removeAttr('disabled');
	}).change();
})
</script>

<form action="" method="post" enctype="multipart/form-data">
<table cellpadding="0" cellspacing="1" bgcolor="#c9c9c9">
	<tr>
		<td></td>
        <td><input type="submit" value="提交" onclick="return uploadForm()" /></td>
    </tr>
    <tr>
        <td>选择html编辑器：</td>
        <td><?php echo Form::select(null,array("kindeditor"=>'kindeditor',"xheditor"=>'xheditor',"ckeditor"=>'ckeditor',"tinymce"=>'tinymce'),isset($_COOKIE['html_editor'])?$_COOKIE['html_editor']:null,array('onchange'=>'change_editor(this)'))?></td>
    </tr>
    <tr>
        <td>标题：</td>
        <td><?php echo Form::input('title')?></td>
    </tr>
    <tr>
        <td>栏目：</td>
        <td><?php echo Form::select('columnId',$object->_config->noselect+$columns)?></td>
    </tr>
    <?php if (1 === $object->user->power):?>
    <tr>
        <td>推荐：</td>
        <td><?php echo Form::checkbox('recommend','1')?>推荐到首页</td>
    </tr>
    <?php endif;?>
    <tr>
        <td>类型：</td>
        <td><?php echo Form::select('type',$object->_config->resourceType)?></td>
    </tr>
    <tr>
        <td>内容：</td>
        <td id="content">
        
        <div class="content" id="art">
        <?php echo $html_editor?>
        
        </div>
        
        <div class="content" id="doc">
        <input type="file" name="upload_content"/>
        *请上传任意文档格式的文件
        </div>
        
        <div class="content" id="zip">
        <input type="file" name="upload_content"/>
        *请上传任意压缩格式的包文件
        </div>
        
        <div class="content" id="mov">
        <input type="file" name="upload_content"/>
        *请上传任意格式的视频文件
        </div>
        
        </td>
    </tr>
    <tr>
        <td></td>
        <td><input type="submit" value="提交" onclick="return uploadForm()" /></td>
    </tr>
</table>
</form>