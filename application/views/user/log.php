<form action="admin/dellog" method="post">
删除<input value="<?php echo date('Y-m-d',strtotime('-60 day'))?>" type="text" id="before" name="before" class="Wdate" onClick="WdatePicker()"/>日之前的记录
<input type="submit" value="删除" onclick="return confirm('确定删除'+$('#before').val()+'日之前的操作日志？将不可恢复')" />
</form>
<?php echo $pagination?>
<table border="0" cellpadding="0" cellspacing="1" bgcolor="#c9c9c9">
	<tr>
		<th class="STYLE1">序号</th>
		<th class="STYLE1">用户名</th>
		<th class="STYLE1">权限</th>
		<th class="STYLE1">行为</th>
		<th class="STYLE1">操作时间</th>
	</tr>
<?php foreach ($logs as $k=>$log):?>
	<tr>
		<td class="STYLE3"><?php echo $k+1?></td>
		<td class="STYLE3"><?php echo $log['username']?></td>
		<td class="STYLE3"><?php echo $object->_config->userPower[$log['power']]?></td>
		<td class="STYLE3"><?php echo $log['action']?></td>
		<td timestamp="timestamp" class="STYLE3"><?php echo date('Y-m-d H:i:s',$log['addTime'])?></td>
	</tr>
<?php endforeach;?>
</table>