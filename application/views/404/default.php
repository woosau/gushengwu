<p class="info">
<strong>错误!</strong>
您遇到了一个程序或者页面的错误
</p><br />

<p class="info">
<strong>地址:</strong>
<?=$exception['url']?></p>

<p class="info">
<strong>文件:</strong>
<?=$exception['file']?></p>

<p class="info">
<strong>行号:</strong>
<?=$exception['line']?></p>

<p class="info">
<strong>信息:</strong>
<?=$exception['message']?></p>

<br />
<p class="info">
<strong>您能做什么？</strong>
<br />-&nbsp;<a href="<?=$referer?>" style="color:green">返回</a>&nbsp;上一页
<br />-&nbsp;用下面链接返回
<br />&nbsp;
<a href="/" style="color:green">所内首页</a>&nbsp;|&nbsp;
<a href="user" style="color:green">后台</a>
</p>