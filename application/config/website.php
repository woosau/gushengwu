<?php
return array(
	'title'=>'::地理',
	//'host'=>URL::base(true).'qtgn/snwynew/',
	'host'=>URL::base(true),
	'noselect'=>array(''=>'==请选择=='),
	'resourceType'=>array('art'=>'文章','doc'=>'文档','zip'=>'压缩文件','mov'=>'视频'),
	'userPower'=>array('1'=>'总管理员','2'=>'子栏目管理员','3'=>'子栏目助手','4'=>'普通用户'),
	'columnPower'=>array('0'=>'读者','1'=>'子栏目管理员','2'=>'子栏目助手')
	//'columnPublic'=>array('0'=>'限制','1'=>'公开')
);