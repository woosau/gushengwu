var cookie = function(){
	this.c();
	this.date = new Date();//日期初始化
}
cookie.prototype = {
	cookies:[],//cookie值存储器
	lifetime:null,//生命周期存储器,单位毫秒
	date:null,//日期存储器
	c:function(){
		cookies_s1 = document.cookie.split('; ');
		for (i in cookies_s1){
			cookies_s2 = cookies_s1[i].split('=');
			this.cookies[cookies_s2[0]] = cookies_s2[1];
		}
	},
	setlifetime:function(lifetime){
		if (lifetime = parseInt(lifetime)){
			this.lifetime = lifetime;
		}
		return this;
	},
	get:function(name){
		this.c();
		if (this.cookies.hasOwnProperty(name)){
			value = decodeURIComponent(this.cookies[name]);
			value = value.replace(/\{\.\,\}/g,';');
			return value;
		}else{
			return false;
		}
	},
	getall:function(){
		return document.cookie;
	},
	set:function(name,value,lifetime){
		value = String(value).replace(/;/g,'{.,}');
		cookieText = name+'='+encodeURIComponent(value)+';';
		lifetime = lifetime || this.lifetime;
		if (lifetime = parseInt(lifetime)){
			this.date.setTime(this.date.getTime()+lifetime*1000);
			cookieText += 'expires='+this.date.toGMTString();
		}
		document.cookie = cookieText;
		return this;
	},
	del:function(name){
		if (this.cookies.hasOwnProperty(name)){
			this.date.setTime(this.date.getTime()-10000);
			document.cookie = name+'=;expires='+this.date.toGMTString();
		}
		return this;
	}
};