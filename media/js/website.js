//打开新窗口
function newWindow(url){
	if (0 > navigator.appName.indexOf('Netscape')){
		if (0 == $('#newWindow').size()){
			$('body').append('<a href="" target="_blank" id="newWindow"></a>');
		}
		document.getElementById('newWindow').href = url;
		document.getElementById('newWindow').click();
	}else{
		window.open(url);
	}
}

//js时间戳格式化成日期格式
function dateformat(timestamp,chinese){
	if (!timestamp){
		datetime = new Date().toLocaleString();
	}else{
		datetime = new Date(parseInt(timestamp)*1000).toLocaleString();
	}
	if (!chinese){
		return datetime.replace(/年|月/g, '-').replace(/日/g, '');
	}
	return datetime;
}

//密码输入确认
function passwordConfirm(objID1,objID2){
	if ($('#'+objID1).val() !== $('#'+objID2).val()){
		alert('重复密码前后不一致，请重填');
		return false;
	}
	return true;
}

//全选反选
function checkall(obj){
	checkboxs = $(obj).parents('table').eq(0).find('input:checkbox');
	for (i in checkboxs){
		checkboxs.get(i).checked=obj.checked;
	}
}
//表格同行全选
trcheck = function(obj){
	checkboxs = $(obj).parents('tr').eq(0).find('input:checkbox');
	for (i in checkboxs){
		checkboxs.get(i).checked=obj.checked;
	}
}
//检查是否已经有选择项
function checkedor(strFormId,strAlert){
	if (0 == $('#'+strFormId).find('input:checkbox:checked[name]:not([name=""])').length){
		alert(strAlert||'尚未选择任何一条记录');
		return false;
	}
	return true;
}
//删除确认
function checkdel(obj,strFormId){
	if (confirm('确定删除所选项？')){
		$('#'+strFormId).submit();
	}
}

//复制到剪贴板
function copyToClipboard(txt) {
	if (window.clipboardData) {
		window.clipboardData.clearData();
		window.clipboardData.setData("Text", txt);
		//alert("复制成功！")
		return true;
	} else if (navigator.userAgent.indexOf("Opera") != -1) {
		window.location = txt;
	} else if (window.netscape) {
		try {
			netscape.security.PrivilegeManager
					.enablePrivilege("UniversalXPConnect");
		} catch (e) {
			alert("被浏览器拒绝！\n请在浏览器地址栏输入'about:config'并回车\n然后将 'signed.applets.codebase_principal_support'设置为'true'");
			return false;
		}
		var clip = Components.classes['@mozilla.org/widget/clipboard;1']
				.createInstance(Components.interfaces.nsIClipboard);
		if (!clip)
			return false;
		var trans = Components.classes['@mozilla.org/widget/transferable;1']
				.createInstance(Components.interfaces.nsITransferable);
		if (!trans)
			return false;
		trans.addDataFlavor('text/unicode');
		var str = new Object();
		var len = new Object();
		var str = Components.classes["@mozilla.org/supports-string;1"]
				.createInstance(Components.interfaces.nsISupportsString);
		var copytext = txt;
		str.data = copytext;
		trans.setTransferData("text/unicode", str, copytext.length * 2);
		var clipid = Components.interfaces.nsIClipboard;
		if (!clip)
			return false;
		clip.setData(trans, null, clipid.kGlobalClipboard);
		//alert("复制成功！");
		return true;
	}
}

$(function(){
	//密码隐藏/显示
	$('span.passwordhide')
	.wrap('<a href="javascript:void(0)" onclick="$(this).hide();$(this).prev().show()"></a>')
	.parents('a')
	.before('<a href="javascript:void(0)" onclick="$(this).hide();$(this).next().show()">******</a>')
	.hide();
	
	//带有timestamp属性的元素内容如果是1970-01-01 08:00:00，即时间戳数值为0，则把内容显示为空
	$('[timestamp=timestamp]').text(function(index,text){
		if ('1970-01-01 08:00:00' == text) return '0000-00-00 00:00:00';
		if ('1970-01-01' == text) return '0000-00-00';
		return text;
	}).css('white-space','nowrap');
	
	//0数字标红
	$('span.redNum').attr('style',function(){
		if (0 == parseInt($(this).text())) return 'color:red';
	});
})